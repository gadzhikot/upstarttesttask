package gadzhikadiev.ramazan.upstarttesttask.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun inflateView(parent: ViewGroup, @LayoutRes res: Int): View =
    LayoutInflater.from(parent.context).inflate(res, parent, false)