package gadzhikadiev.ramazan.upstarttesttask.ui.dress_details

import gadzhikadiev.ramazan.upstarttesttask.interactor.CatalogueInteracorImpl
import gadzhikadiev.ramazan.upstarttesttask.ui.base.BasePresenter
import javax.inject.Inject

class Presenter @Inject constructor(
    private val interacor: CatalogueInteracorImpl
) : BasePresenter<Contract.View>(), Contract.Presenter {

    override fun viewCreated() {
        view?.let {
            val id = it.getDressId()
            val isFavorite = interacor.isFavorite(id)
            it.fillFavorite(isFavorite)
        }
    }

    override fun favoriteClicked(id: Int, isChecked: Boolean) {
        interacor.addFavorite(id, isChecked)
        if (isChecked)
            view?.showAddedToFavoritesMessage()
        else
            view?.showRemovedFromFavoritesMessage()
    }
}