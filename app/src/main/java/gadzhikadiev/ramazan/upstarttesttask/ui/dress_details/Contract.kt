package gadzhikadiev.ramazan.upstarttesttask.ui.dress_details

import gadzhikadiev.ramazan.upstarttesttask.ui.base.MvpContract

interface Contract {
    interface View : MvpContract.View {

        fun getDressId(): Int

        fun fillFavorite(isFavorite: Boolean)

        fun showAddedToFavoritesMessage()

        fun showRemovedFromFavoritesMessage()

    }

    interface Presenter : MvpContract.Presenter<View> {

        fun favoriteClicked(id: Int, isChecked: Boolean)

    }
}