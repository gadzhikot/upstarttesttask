package gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.androidadvance.topsnackbar.TSnackbar
import gadzhikadiev.ramazan.upstarttesttask.App
import gadzhikadiev.ramazan.upstarttesttask.R
import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import gadzhikadiev.ramazan.upstarttesttask.ui.base.FragmentNavigator
import gadzhikadiev.ramazan.upstarttesttask.ui.base.Stylable
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_details.DressDetailsFragment
import gadzhikadiev.ramazan.upstarttesttask.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_dress_catalogue.*
import javax.inject.Inject

class DressCatalogueFragment : Fragment(), Contract.View {

    @Inject
    lateinit var presenter: Contract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.applicationComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(
        R.layout.fragment_dress_catalogue,
        container,
        false
    ).apply {
        presenter.attachView(this@DressCatalogueFragment)
        makeNonTransparentStatusBar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initDressList()
        presenter.viewCreated()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun showDresses(dresses: List<Dress>) {
        list_dress_catalogue?.adapter =
            DressCatalogueAdapter(
                dresses.toMutableList(),
                presenter::itemClicked,
                presenter::favoriteClicked,
                presenter::onBindDress
            )
    }

    override fun showErrorMessage() {
        TODO("Not yet implemented")
    }

    override fun showConnectionErrorMessage() {
        TODO("Not yet implemented")
    }

    override fun showAddedToFavoritesMessage() {
        showSnackBar(R.string.dress_catalogue_added_to_favorites)
    }

    override fun showRemovedFromFavoritesMessage() {
        showSnackBar(R.string.dress_catalogue_removed_from_favorites)
    }

    override fun navigateToDetailedScreen(dress: Dress) {
        val navigator = this.context
        if (navigator is FragmentNavigator) {
            navigator.navigateTo(
                DressDetailsFragment(dress),
                true
            )
        }
    }

    private fun makeNonTransparentStatusBar() {
        val stylable = context
        if (stylable is Stylable)
            stylable.makeTransperantStatusBar(false)
    }

    private fun initDressList() {
        val spanCount = 2
        val firstPosition = 0

        list_dress_catalogue?.layoutManager = GridLayoutManager(context, spanCount).apply {
            spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int =
                    if (position == firstPosition) 2
                    else 1
            }
        }
    }

    private fun showSnackBar(@StringRes resId: Int) {
        TSnackbar.make(
            list_dress_catalogue,
            resId,
            TSnackbar.LENGTH_SHORT
        ).apply {
            view.setBackgroundColor(
                getColor(R.color.dark_gray)
            )
            view.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)?.apply {
                setTextColor(getColor(R.color.white))
                gravity = Gravity.CENTER_HORIZONTAL
            }
        }.show()
    }

    private fun getColor(@ColorRes resId: Int) =
        ContextCompat.getColor(context!!, resId)
}