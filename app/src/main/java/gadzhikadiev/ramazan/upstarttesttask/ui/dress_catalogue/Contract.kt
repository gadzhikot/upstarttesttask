package gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue

import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import gadzhikadiev.ramazan.upstarttesttask.ui.base.MvpContract

interface Contract {
    interface View : MvpContract.View {
        fun showDresses(dresses: List<Dress>)

        fun showErrorMessage()

        fun showConnectionErrorMessage()

        fun showAddedToFavoritesMessage()

        fun showRemovedFromFavoritesMessage()

        fun navigateToDetailedScreen(dress: Dress)
    }

    interface Presenter : MvpContract.Presenter<View> {

        fun itemClicked(id: Int)

        fun favoriteClicked(id: Int, isChecked: Boolean)

        fun onBindDress(id: Int): Boolean
    }
}