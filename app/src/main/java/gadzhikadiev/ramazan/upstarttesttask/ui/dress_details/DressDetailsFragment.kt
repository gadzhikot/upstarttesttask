package gadzhikadiev.ramazan.upstarttesttask.ui.dress_details

import android.app.Activity
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.androidadvance.topsnackbar.TSnackbar
import com.bumptech.glide.Glide
import gadzhikadiev.ramazan.upstarttesttask.App
import gadzhikadiev.ramazan.upstarttesttask.R
import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import gadzhikadiev.ramazan.upstarttesttask.ui.base.Stylable
import kotlinx.android.synthetic.main.fragment_dress_details.*
import javax.inject.Inject

class DressDetailsFragment() : Fragment(), Contract.View {

    private val dressRetainKey = "dress"

    private var dress: Dress? = null

    constructor(dress: Dress) : this() {
        this.dress = dress
    }

    @Inject
    lateinit var presenter: Contract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.applicationComponent.inject(this)

        if (savedInstanceState != null && savedInstanceState.containsKey(dressRetainKey)) {
            dress = savedInstanceState.getParcelable(dressRetainKey)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(
        R.layout.fragment_dress_details,
        container,
        false
    ).apply {
        presenter.attachView(this@DressDetailsFragment)
        makeTransparentStatusBar()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.viewCreated()
        initBackButton()
        dress?.let {
            fillImage(it.image)
            fillTitle(it.title)
            fillPrice(it.price.toString())
            fillIsFavorite(it.id)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(dressRetainKey, dress)
        super.onSaveInstanceState(outState)
    }

    override fun getDressId(): Int = dress?.id ?: -1

    override fun fillFavorite(isFavorite: Boolean) {
        toggle_dress_details_is_favorite.apply {
            isChecked = isFavorite
        }
    }

    override fun showAddedToFavoritesMessage() {
        showSnackBar(R.string.dress_catalogue_added_to_favorites)
    }

    override fun showRemovedFromFavoritesMessage() {
        showSnackBar(R.string.dress_catalogue_removed_from_favorites)
    }


    private fun makeTransparentStatusBar() {
        val stylable = context
        if (stylable is Stylable)
            stylable.makeTransperantStatusBar(true)
    }

    private fun initBackButton() {
        val activity = context
        iv_dress_details_back.setOnClickListener{
            if (activity is Activity) activity.onBackPressed()
        }
    }

    private fun fillTitle(title: String) {
        tv_dress_details_name.text = title
    }

    private fun fillPrice(price: String) {
        tv_dress_details_price.text = price
    }

    private fun fillImage(url: String) {
        Glide.with(context!!).load(url).into(iv_dress_details_image)
    }

    private fun fillIsFavorite(id: Int) {
        toggle_dress_details_is_favorite.setOnCheckedChangeListener { _, isChecked ->
            presenter.favoriteClicked(id, isChecked)
        }
    }

    private fun showSnackBar(@StringRes resId: Int) {
        TSnackbar.make(
            iv_dress_details_back,
            resId,
            TSnackbar.LENGTH_SHORT
        ).apply {
            view.setBackgroundColor(
                getColor(R.color.dark_gray)
            )
            view.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text)?.apply {
                setTextColor(getColor(R.color.white))
                gravity = Gravity.CENTER_HORIZONTAL
            }
        }.show()
    }

    private fun getColor(@ColorRes resId: Int) =
        ContextCompat.getColor(context!!, resId)
}