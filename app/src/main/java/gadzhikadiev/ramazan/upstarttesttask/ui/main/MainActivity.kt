package gadzhikadiev.ramazan.upstarttesttask.ui.main

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import gadzhikadiev.ramazan.upstarttesttask.R
import gadzhikadiev.ramazan.upstarttesttask.ui.base.FragmentNavigator
import gadzhikadiev.ramazan.upstarttesttask.ui.base.Stylable
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue.DressCatalogueFragment


class MainActivity : AppCompatActivity(), FragmentNavigator, Stylable {

    override fun onCreate(savedInstanceState: Bundle?) {
        setLightStatusBarForApiLower23()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null)
            navigateTo(DressCatalogueFragment(), false)
    }

    override fun navigateTo(fragment: Fragment, addToBackStack: Boolean) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.layout_main_container, fragment).apply {
                if (addToBackStack) addToBackStack(fragment.tag)
            }.commit()
    }

    private fun setLightStatusBarForApiLower23() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            window.statusBarColor = ContextCompat.getColor(this, R.color.black)
            window.statusBarColor = Color.TRANSPARENT
        }
    }

    override fun makeTransperantStatusBar(isTransperant: Boolean) {
        if (isTransperant)
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            else
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    }
}