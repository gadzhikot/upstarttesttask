package gadzhikadiev.ramazan.upstarttesttask.ui.base

import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment() {

    protected fun navigateTo(destination: Fragment, addToBackStack: Boolean) {
        if (context is FragmentNavigator) {
            (context as FragmentNavigator).navigateTo(destination, addToBackStack)
        }
    }
}