package gadzhikadiev.ramazan.upstarttesttask.ui.base

interface MvpContract {

    interface View

    interface Presenter<V : View> {

        fun attachView(view: V)

        fun detachView()

        fun viewCreated()

    }

}