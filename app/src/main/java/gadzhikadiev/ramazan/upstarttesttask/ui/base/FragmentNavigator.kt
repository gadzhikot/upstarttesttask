package gadzhikadiev.ramazan.upstarttesttask.ui.base

import androidx.fragment.app.Fragment

interface FragmentNavigator {
    fun navigateTo(fragment:Fragment, addToBackStack: Boolean)
}