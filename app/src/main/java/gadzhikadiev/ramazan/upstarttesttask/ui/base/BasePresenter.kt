package gadzhikadiev.ramazan.upstarttesttask.ui.base

abstract class BasePresenter<T : MvpContract.View> : MvpContract.Presenter<T> {

    protected var view: T? = null
        private set

    override fun attachView(view: T) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}