package gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue

import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import gadzhikadiev.ramazan.upstarttesttask.interactor.CatalogueInteracorImpl
import gadzhikadiev.ramazan.upstarttesttask.ui.base.BasePresenter
import javax.inject.Inject

class Presenter @Inject constructor(
    private val dresses: MutableList<Dress>,
    private val interacor: CatalogueInteracorImpl
) :
    BasePresenter<Contract.View>(), Contract.Presenter {

    override fun viewCreated() {
        view?.let { view ->
            if (dresses.isNullOrEmpty()) {
                val onSuccess: (List<Dress>) -> Unit = {
                    this.dresses.addAll(it)
                    view.showDresses(it)
                }
                val onError: () -> Unit = {
                    view.showErrorMessage()
                }
                val onFailure: () -> Unit = {
                    view.showConnectionErrorMessage()
                }

                interacor.requestCatalogue(onSuccess, onError, onFailure)
            } else {
                view.showDresses(dresses)
            }
        }
    }

    override fun itemClicked(id: Int) {
        dresses.first { it.id == id }.let {
            view?.navigateToDetailedScreen(it)
        }
    }

    override fun favoriteClicked(id: Int, isChecked: Boolean) {
        interacor.addFavorite(id, isChecked)
        if (isChecked)
            view?.showAddedToFavoritesMessage()
        else
            view?.showRemovedFromFavoritesMessage()
    }

    override fun onBindDress(id: Int) =
        interacor.isFavorite(id)
}