package gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import gadzhikadiev.ramazan.upstarttesttask.R
import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import gadzhikadiev.ramazan.upstarttesttask.util.inflateView
import kotlinx.android.synthetic.main.item_dress_catalogue.view.*
import kotlinx.android.synthetic.main.item_dress_catalogue_counter.view.*

class DressCatalogueAdapter(
    private val dresses: MutableList<Dress>,
    private val onItemClick: (Int) -> Unit,
    private val onFavoriteClick: (Int, Boolean) -> Unit,
    private val onBindDress: (Int) -> Boolean
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val headerViewType = 0
    private val dressViewType = 1

    override fun getItemViewType(position: Int): Int =
        if (position == 0) headerViewType
        else dressViewType

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        when (viewType) {
            headerViewType -> HeaderViewHolder(
                inflateView(parent, R.layout.item_dress_catalogue_counter)
            )
            else -> DressViewHolder(
                inflateView(parent, R.layout.item_dress_catalogue)
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DressViewHolder -> dresses[position - 1].apply {
                holder.bind(this, onItemClick, onFavoriteClick, onBindDress)
            }
            is HeaderViewHolder -> holder.bind(dresses.size)
        }
    }

    override fun getItemCount() = dresses.size + 1

    class DressViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(
            dress: Dress,
            onItemClick: (Int) -> Unit,
            onFavoriteClick: (Int, Boolean) -> Unit,
            onBindingDress: (Int) -> Boolean
        ) {
            itemView.tv_dress_catalogue_name.text = dress.title
            itemView.tv_dress_catalogue_price.text = dress.price.toString()
            itemView.tv_dress_catalogue_new_attribute.visibility =
                if (dress.new) View.VISIBLE else View.INVISIBLE

            Glide.with(itemView)
                .load(dress.image)
                .into(itemView.iv_dress_catalogue_preview)

            itemView.setOnClickListener { onItemClick(dress.id) }

            itemView.toggle_dress_catalogue_is_favorite.isChecked = onBindingDress(dress.id)

            itemView.toggle_dress_catalogue_is_favorite.setOnCheckedChangeListener { _, isChecked ->
                onFavoriteClick(dress.id, isChecked)
            }
        }
    }

    class HeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(size: Int) {
            itemView.tv_dress_catalogue_message.text =
                itemView.context.getString(R.string.dress_catalogue_products_count, size)
        }
    }

}