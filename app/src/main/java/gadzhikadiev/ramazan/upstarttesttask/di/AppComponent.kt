package gadzhikadiev.ramazan.upstarttesttask.di

import android.app.Application
import dagger.Component
import gadzhikadiev.ramazan.upstarttesttask.di.modules.AppModule
import gadzhikadiev.ramazan.upstarttesttask.di.modules.CatalogueModule
import gadzhikadiev.ramazan.upstarttesttask.di.modules.DetailsModule
import gadzhikadiev.ramazan.upstarttesttask.di.modules.NetworkModule
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue.DressCatalogueFragment
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_details.DressDetailsFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, CatalogueModule::class, DetailsModule::class])
interface AppComponent {
    val application: Application

    fun inject(dressCatalogueFragment: DressCatalogueFragment)

    fun inject(dressDetailsFragment: DressDetailsFragment)

}