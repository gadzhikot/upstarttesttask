package gadzhikadiev.ramazan.upstarttesttask.di.modules

import dagger.Binds
import dagger.Module
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_details.Contract
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_details.Presenter

@Module
interface DetailsModule {

    @Binds
    fun bindPresenter(presenter: Presenter): Contract.Presenter
}