package gadzhikadiev.ramazan.upstarttesttask.di.modules

import dagger.Binds
import dagger.Module
import dagger.Provides
import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import gadzhikadiev.ramazan.upstarttesttask.interactor.CatalogueInteractor
import gadzhikadiev.ramazan.upstarttesttask.repository.CatalogueRepository
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue.Contract
import gadzhikadiev.ramazan.upstarttesttask.interactor.CatalogueInteracorImpl
import gadzhikadiev.ramazan.upstarttesttask.repository.CatalogueRepositoryImpl
import gadzhikadiev.ramazan.upstarttesttask.ui.dress_catalogue.Presenter

@Module
interface CatalogueModule {

    companion object {

        @Provides
        fun provideDressList() = mutableListOf<Dress>()

    }
    @Binds
    fun bindDressCataloguePresenter(presenter: Presenter): Contract.Presenter

    @Binds
    fun bindCatalogueInteractor(interactor: CatalogueInteracorImpl): CatalogueInteractor

    @Binds
    fun bindCatalogueRepository(repository: CatalogueRepositoryImpl): CatalogueRepository

}