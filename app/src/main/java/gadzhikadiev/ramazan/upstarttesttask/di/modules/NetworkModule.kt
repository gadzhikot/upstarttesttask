package gadzhikadiev.ramazan.upstarttesttask.di.modules

import dagger.Module
import dagger.Provides
import gadzhikadiev.ramazan.upstarttesttask.data.network.RestService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
interface NetworkModule {
    companion object {
        @Provides
        @Singleton
        fun provideHttpLoggingInterceptor() =
            HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        @Provides
        @Singleton
        fun providesOkHttpClient(interceptor: HttpLoggingInterceptor) =
            OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()

        @Provides
        @Singleton
        fun provideConverterFactory(): GsonConverterFactory =
            GsonConverterFactory.create()

        @Provides
        @Singleton
        fun provideRetrofit(
            okHttpClient: OkHttpClient,
            converterFactory: GsonConverterFactory
        ): Retrofit =
            Retrofit.Builder()
                .baseUrl(RestService.BASE_URL)
                .addConverterFactory(converterFactory)
                .client(okHttpClient)
                .build()

        @Provides
        @Singleton
        fun provideRestService(retrofit: Retrofit): RestService =
            retrofit.create(RestService::class.java)
    }
}