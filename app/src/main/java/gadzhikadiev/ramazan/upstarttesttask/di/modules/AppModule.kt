package gadzhikadiev.ramazan.upstarttesttask.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val mApplication: Application) {

    @Provides
    @Singleton
    fun provideApplication() = mApplication

    @Provides
    @Singleton
    fun provideSharedPreferences() = mApplication.getSharedPreferences(
        mApplication.packageName,
        Context.MODE_PRIVATE
    )
}