package gadzhikadiev.ramazan.upstarttesttask.repository

import gadzhikadiev.ramazan.upstarttesttask.data.DataManager
import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class CatalogueRepositoryImpl @Inject constructor(private val dataManager: DataManager) :
    CatalogueRepository {

    override fun select(
        onSuccess: (List<Dress>) -> Unit,
        onError: () -> Unit,
        onFailure: () -> Unit
    ) {
        dataManager.getJeans().enqueue(object : Callback<List<Dress>> {
            override fun onFailure(call: Call<List<Dress>>, t: Throwable) {
                onFailure()
            }

            override fun onResponse(call: Call<List<Dress>>, response: Response<List<Dress>>) {
                if (response.isSuccessful) response.body()
                    ?.let(onSuccess) ?: onSuccess(listOf())
                else onError()
            }
        })
    }

    override fun addToFavorites(id: Int) {
        dataManager.addToFavorites(id)
    }

    override fun isFavorite(id: Int) =
        dataManager.isFavorite(id)

    override fun removeFavorite(id: Int) {
        dataManager.removeFromFavorites(id)
    }


}