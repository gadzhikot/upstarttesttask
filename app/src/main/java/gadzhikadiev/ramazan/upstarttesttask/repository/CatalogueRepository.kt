package gadzhikadiev.ramazan.upstarttesttask.repository

import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress

interface CatalogueRepository {
    fun select(
        onSuccess: (List<Dress>) -> Unit,
        onError: () -> Unit,
        onFailure: () -> Unit
    )

    fun addToFavorites(id: Int)

    fun isFavorite(id: Int): Boolean

    fun removeFavorite(id: Int)
}