package gadzhikadiev.ramazan.upstarttesttask

import android.app.Application
import gadzhikadiev.ramazan.upstarttesttask.di.AppComponent
import gadzhikadiev.ramazan.upstarttesttask.di.DaggerAppComponent
import gadzhikadiev.ramazan.upstarttesttask.di.modules.AppModule

class App : Application() {
    companion object {
        lateinit var applicationComponent: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        applicationComponent = createAppComponent()
    }

    private fun createAppComponent() =
        DaggerAppComponent.builder()
            .appModule(
                AppModule(this)
            )
            .build()
}