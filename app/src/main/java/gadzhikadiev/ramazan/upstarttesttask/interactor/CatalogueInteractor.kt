package gadzhikadiev.ramazan.upstarttesttask.interactor

import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress

interface CatalogueInteractor {

    fun requestCatalogue(
        onSuccess: (List<Dress>) -> Unit,
        onError: () -> Unit,
        onFailure: () -> Unit
    )

    fun addFavorite(id: Int, addToFavorites: Boolean)

    fun isFavorite(id: Int): Boolean
}