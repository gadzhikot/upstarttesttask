package gadzhikadiev.ramazan.upstarttesttask.interactor

import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import gadzhikadiev.ramazan.upstarttesttask.interactor.CatalogueInteractor
import gadzhikadiev.ramazan.upstarttesttask.repository.CatalogueRepository
import javax.inject.Inject

class CatalogueInteracorImpl @Inject constructor(
    private val repository: CatalogueRepository
) :
    CatalogueInteractor {

    override fun requestCatalogue(
        onSuccess: (List<Dress>) -> Unit,
        onError: () -> Unit,
        onFailure: () -> Unit
    ) {
        repository.select(onSuccess, onError, onFailure)
    }

    override fun addFavorite(id: Int, addToFavorites: Boolean) {
        if (addToFavorites)
            repository.addToFavorites(id)
        else
            repository.removeFavorite(id)
    }

    override fun isFavorite(id: Int) =
        repository.isFavorite(id)
}