package gadzhikadiev.ramazan.upstarttesttask.data.network

import gadzhikadiev.ramazan.upstarttesttask.data.model.Dress
import retrofit2.Call
import retrofit2.http.GET

interface RestService {
    companion object {
        const val BASE_URL = "https://static.upstarts.work/"
    }

    @GET("tests/jeans/jeans-default.json")
    fun getJeans() : Call<List<Dress>>
}