package gadzhikadiev.ramazan.upstarttesttask.data

import android.content.SharedPreferences
import gadzhikadiev.ramazan.upstarttesttask.data.network.RestService
import javax.inject.Inject

class DataManager @Inject constructor(
    private val service: RestService,
    private val preferences: SharedPreferences
) {

    val favoritesKey = { id: Int -> "DRESS_$id" }

    fun getJeans() = service.getJeans()

    fun addToFavorites(id: Int) {
        preferences.edit()
            .putBoolean(
                favoritesKey(id),
                true
            ).apply()
    }

    fun isFavorite(id: Int) =
        preferences.getBoolean(favoritesKey(id), false)

    fun removeFromFavorites(id: Int) {
        preferences.edit().remove(
            favoritesKey(id)
        ).apply()
    }
}