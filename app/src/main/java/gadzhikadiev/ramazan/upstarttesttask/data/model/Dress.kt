package gadzhikadiev.ramazan.upstarttesttask.data.model

import android.os.Parcel
import android.os.Parcelable
import javax.inject.Inject

data class Dress @Inject constructor(
    val id: Int,
    val image: String,
    val new: Boolean,
    val price: Double,
    val title: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString() ?: "",
        parcel.readByte() != 0.toByte(),
        parcel.readDouble(),
        parcel.readString() ?: ""
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(image)
        parcel.writeByte(if (new) 1 else 0)
        parcel.writeDouble(price)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Dress> {
        override fun createFromParcel(parcel: Parcel): Dress {
            return Dress(parcel)
        }

        override fun newArray(size: Int): Array<Dress?> {
            return arrayOfNulls(size)
        }
    }
}